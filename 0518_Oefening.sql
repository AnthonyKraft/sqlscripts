Use ModernWays;
set SQL_SAFE_UPDATES = 0;
Alter table Huisdieren add column Geluid VARCHAR(20);
Update Huisdieren SET Geluid = "WAF!" Where Soort = "Hond";
Update Huisdieren SET Geluid = "miauwww..." Where Soort = "Kat";
set SQL_SAFE_UPDATES = 1;

